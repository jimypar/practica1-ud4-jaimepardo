package main;

import gui.Controlador;
import gui.Modelo;
import gui.Vista;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Jaime on 13/12/2021.
 */
public class Principal {
    public static void main(String[] args) {
        tema();
        Modelo modelo = new Modelo();
        Vista vista = new Vista();
        Controlador controlador = new Controlador(vista, modelo);
    }

    /**
     * Metodo que asigna el tema y los colores de la Vista.
     */
    public static void tema() {
        UIManager.put( "control", new Color(105, 87, 117) );
        UIManager.put( "info", new Color(103, 70,128) );
        UIManager.put( "nimbusBase", new Color(70, 4, 69) );
        UIManager.put( "nimbusLightBackground", new Color(83, 59, 99) );
        UIManager.put( "nimbusSelectedText", new Color( 255, 255, 255) );
        UIManager.put( "nimbusSelectionBackground", new Color(156, 126, 155) );
        UIManager.put( "text", new Color(255, 255, 255) );
        try {
            for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
        }

    }
}
