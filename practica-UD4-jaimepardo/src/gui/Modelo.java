package gui;

import com.jaimepardo.practica1.*;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import util.Util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Jaime Pardo on 13/12/2021.
 */
public class Modelo {
    private final static String DATABASE="TallerJaime";
    private final static String COLECCION_COCHES = "Coches";
    private final static String COLECCION_CLIENTES = "Clientes";
    private final static String COLECCION_MECANICOS = "Mecanicos";
    private final static String COLECCION_RECAMBIOS = "Recambios";

    private MongoClient client;
    private MongoDatabase baseDatos;
    private MongoCollection coleccionCoches;
    private MongoCollection coleccionClientes;
    private MongoCollection coleccionMecanicos;
    private MongoCollection coleccionRecambios;

    /**
     * Metodo que conecta con la base de datos
     *
     * @param vista
     */
    void conectar(Vista vista) {
        client=new MongoClient();
        baseDatos=client.getDatabase(DATABASE);
        coleccionCoches=baseDatos.getCollection(COLECCION_COCHES);
        coleccionClientes=baseDatos.getCollection(COLECCION_CLIENTES);
        coleccionMecanicos=baseDatos.getCollection(COLECCION_MECANICOS);
        coleccionRecambios=baseDatos.getCollection(COLECCION_RECAMBIOS);

        vista.panel1.setVisible(true);
    }

    /**
     * Metodo que desconecta la base
     */
    void desconectar() {
        client.close();
    }

    /**
     * Metodo que inserta Cliente
     */
    void insertarCliente(Cliente cliente) {
        coleccionClientes.insertOne(clienteToDocument(cliente));
    }

    /**
     * Metodo que inserta Mecanico
     */
    void insertarMecanico(Mecanico mecanico) {
        coleccionMecanicos.insertOne(mecanicoToDocument(mecanico));
    }

    /**
     * Metodo que inserta Coche
     */
    void insertarCoche(Coche coche)  {
        coleccionCoches.insertOne(cocheToDocument(coche));
    }

    /**
     * Metodo que inserta un recambio
     */
    public void insertarRecambio(Recambio recambio)  {
        coleccionRecambios.insertOne(recambioToDocument(recambio));

    }

    /**
     * Metodo que modifica un cliente
     */
    void modificarCliente(Cliente cliente) {
        coleccionClientes.replaceOne(new Document("_id",cliente.getId()),
                clienteToDocument(cliente));
    }

    /**
     * Metodo que modifica un mecanico
     */
    void modificarMecanico(Mecanico mecanico) {
        coleccionMecanicos.replaceOne(new Document("_id",mecanico.getId()),
                mecanicoToDocument(mecanico));
    }

    /**
     * Metodo que modifica un coche
     */
    void modificarCoche(Coche coche) {
        coleccionCoches.replaceOne(new Document("_id",coche.getId()),
                cocheToDocument(coche));
    }

    /**
     * Metodo que modifica un recambio
     */
    public void modificarRecambio(Recambio recambio) {
        coleccionRecambios.replaceOne(new Document("_id",recambio.getId()),
                recambioToDocument(recambio));
    }

    /**
     * Metodo que borra un mecanico
     */
    void borrarMecanico(Mecanico mecanico) {
        coleccionMecanicos.deleteOne(mecanicoToDocument(mecanico));
    }

    /**
     * Metodo que borra un cliente
     */
    void borrarCliente(Cliente cliente)  {
        coleccionClientes.deleteOne(clienteToDocument(cliente));
    }

    /**
     * Metodo que borra un coche
     */
    void borrarCoche(Coche coche) {
        coleccionCoches.deleteOne(cocheToDocument(coche));
    }

    /**
     * Metodo que borra un recambio
     */
    void borrarRecambio(Recambio recambio) {
        coleccionRecambios.deleteOne(recambioToDocument(recambio));
    }

    /**
     * Metodo que devuelve todos los coches
     *
     * @return
     */
    public List<Coche> getCoches() {
        ArrayList<Coche> lista = new ArrayList<>();
        Iterator<Document> it = coleccionCoches.find().iterator();
        while (it.hasNext()) {
            lista.add(documentToCoche(it.next()));
        }
        return lista;
    }


    /**
     * Metodo que devuelve todos los clientes
     *
     * @return
     */
    public List<Cliente> getClientes() {
        ArrayList<Cliente> lista = new ArrayList<>();
        Iterator<Document> it = coleccionClientes.find().iterator();
        while (it.hasNext()) {
            lista.add(documentToCliente(it.next()));
        }
        return lista;
    }

    /**
     * Metodo que devuelve el cliente de un coche
     *
     * @return
     */
    public Cliente getCliente(Coche cocheSeleccion) {
        Document documento = new Document("_id", cocheSeleccion.getClienteId());
        Document cliente = (Document) coleccionClientes.find(documento).first();
        return documentToCliente(cliente);
    }


    /**
     * Metodo que devuelve todos los recambios
     *
     * @return
     */
    public List<Recambio> getRecambio() {
        ArrayList<Recambio> lista = new ArrayList<>();
        Iterator<Document> it = coleccionRecambios.find().iterator();
        while (it.hasNext()) {
            lista.add(documentToRecambio(it.next()));
        }
        return lista;
    }

    /**
     * Metodo que devuelve el recambio de un coche
     *
     * @return
     */
    public Recambio getRecambio(Coche cocheSeleccion) {
        Document documento = new Document("_id", cocheSeleccion.getRecambioId());
        Document recambio = (Document) coleccionRecambios.find(documento).first();
        return documentToRecambio(recambio);
    }

    /**
     * Metodo que devuelve todos los mecanicos
     *
     * @return
     */
    public List<Mecanico> getMecanicos() {
        ArrayList<Mecanico> lista = new ArrayList<>();
        Iterator<Document> it = coleccionMecanicos.find().iterator();
        while (it.hasNext()) {
            lista.add(documentToMecanico(it.next()));
        }
        return lista;
    }

    /**
     * Metodo que devuelve el mecanico de un coche
     *
     * @return
     */
    public Mecanico getMecanico(Coche cocheSeleccion) {
        Document documento = new Document("_id", cocheSeleccion.getMecanicoId());
        Document mecanico = (Document) coleccionMecanicos.find(documento).first();
        return documentToMecanico(mecanico);
    }

    /**
     * Metodo que devuelve los coches buscados
     *
     * @return
     */
    public List<Coche> getCoches(String text) {
        ArrayList<Coche> lista = new ArrayList<>();
        Document query = new Document();
        List<Document> listaCriterios = new ArrayList<>();
        listaCriterios.add(new Document("matricula",new Document("$regex","/*"+text+"/*")));
        listaCriterios.add(new Document("marca",new Document("$regex","/*"+text+"/*")));
        query.append("$or",listaCriterios);
        Iterator<Document> iterator =coleccionCoches.find(query).iterator();
        while (iterator.hasNext()) {
            lista.add(documentToCoche(iterator.next()));
        }
        return lista;
    }

    /**
     * Metodo que devuelve los clientes buscados
     *
     * @return
     */
    public List<Cliente> getClientes(String text) {
        ArrayList<Cliente> lista = new ArrayList<>();
        Document query = new Document();
        List<Document> listaCriterios = new ArrayList<>();
        listaCriterios.add(new Document("nombre",new Document("$regex","/*"+text+"/*")));
        query.append("$or",listaCriterios);
        Iterator<Document> iterator =coleccionClientes.find(query).iterator();
        while (iterator.hasNext()) {
            lista.add(documentToCliente(iterator.next()));
        }
        return lista;
    }

    /**
     * Metodo que devuelve los recambios buscados
     *
     * @return
     */
    public List<Recambio> getRecambios(String text) {
        ArrayList<Recambio> lista = new ArrayList<>();
        Document query = new Document();
        List<Document> listaCriterios = new ArrayList<>();
        listaCriterios.add(new Document("componente",new Document("$regex","/*"+text+"/*")));
        query.append("$or",listaCriterios);
        Iterator<Document> iterator =coleccionRecambios.find(query).iterator();
        while (iterator.hasNext()) {
            lista.add(documentToRecambio(iterator.next()));
        }
        return lista;
    }

    /**
     * Metodo que devuelve los mecanicos buscados
     *
     * @return
     */
    public List<Mecanico> getMecanicos(String text) {
        ArrayList<Mecanico> lista = new ArrayList<>();
        Document query = new Document();
        List<Document> listaCriterios = new ArrayList<>();
        listaCriterios.add(new Document("nombre",new Document("$regex","/*"+text+"/*")));
        query.append("$or",listaCriterios);
        Iterator<Document> iterator =coleccionMecanicos.find(query).iterator();
        while (iterator.hasNext()) {
            lista.add(documentToMecanico(iterator.next()));
        }
        return lista;
    }

    /**
     * Transforma un coche en un documento
     *
     * @param unCoche
     * @return
     */
    public Document cocheToDocument(Coche unCoche) {
        Document documento = new Document();
        documento.append("tipo",unCoche.getTipo());
        documento.append("matricula",unCoche.getMatricula());
        documento.append("marca",unCoche.getMarca());
        documento.append("fechaAlta", Util.formatearFecha(unCoche.getFechaAlta()));
        documento.append("clienteId", unCoche.getClienteId());
        documento.append("recambioId", unCoche.getRecambioId());
        documento.append("mecanicoId", unCoche.getMecanicoId());
        return documento;
    }

    /**
     *
     * Transforma un documento en un coche
     *
     * @param document
     * @return
     */
    public Coche documentToCoche(Document document) {
        Coche unCoche = new Coche();
        unCoche.setId(document.getObjectId("_id"));
        unCoche.setTipo(document.getString("tipo"));
        unCoche.setMatricula(document.getString("matricula"));
        unCoche.setMarca(document.getString("marca"));
        unCoche.setFechaAlta(Util.parsearFecha(document.getString("fechaAlta")));
        unCoche.setClienteId(document.getObjectId("clienteId"));
        unCoche.setRecambioId(document.getObjectId("recambioId"));
        unCoche.setMecanicoId(document.getObjectId("mecanicoId"));
        return unCoche;
    }

    /**
     * Transforma un cliente en un documento
     *
     * @param cliente
     * @return
     */
    public Document clienteToDocument(Cliente cliente) {
        Document documento = new Document();
        documento.append("dni",cliente.getDni());
        documento.append("nombre",cliente.getNombre());
        documento.append("apellidos",cliente.getApellidos());
        documento.append("email",cliente.getEmail());
        documento.append("telefono",cliente.getTelefono());
        return documento;
    }

    /**
     *
     * Transforma un documento en un cliente
     *
     * @param document
     * @return
     */
    public Cliente documentToCliente(Document document) {
        Cliente cliente = new Cliente();
        cliente.setId(document.getObjectId("_id"));
        cliente.setDni(document.getString("dni"));
        cliente.setNombre(document.getString("nombre"));
        cliente.setApellidos(document.getString("apellidos"));
        cliente.setEmail(document.getString("email"));
        cliente.setTelefono(document.getInteger("telefono"));
        return cliente;
    }

    /**
     *
     * Transforma un recambio en un documento
     *
     * @param recambio
     * @return
     */
    public Document recambioToDocument(Recambio recambio) {
        Document documento = new Document();
        documento.append("componente",recambio.getComponente());
        documento.append("precio",recambio.getPrecio());
        documento.append("combustion",recambio.getCombustion());
        documento.append("electrico",recambio.getElectrico());
        return documento;
    }

    /**
     *
     * Transforma un documento en un recambio
     *
     * @param document
     * @return
     */
    public Recambio documentToRecambio(Document document) {
        Recambio recambio = new Recambio();
        recambio.setId(document.getObjectId("_id"));
        recambio.setComponente(document.getString("componente"));
        recambio.setPrecio(document.getDouble("precio"));
        recambio.setCombustion(document.getBoolean("combustion"));
        recambio.setElectrico(document.getBoolean("electrico"));
        return recambio;
    }


    /**
     *
     * Transforma un mecanico en un documento
     *
     * @param mecanico
     * @return
     */
    public Document mecanicoToDocument(Mecanico mecanico) {
        Document documento = new Document();
        documento.append("nombre",mecanico.getNombre());
        documento.append("apellidos",mecanico.getApellidos());
        documento.append("telefono",mecanico.getTelefono());
        return documento;
    }

    /**
     *
     * Trasforma un documento en un mecancico
     *
     * @param document
     * @return
     */
    public Mecanico documentToMecanico(Document document) {
        Mecanico mecanico = new Mecanico();
        mecanico.setId(document.getObjectId("_id"));
        mecanico.setNombre(document.getString("nombre"));
        mecanico.setApellidos(document.getString("apellidos"));
        mecanico.setTelefono(document.getInteger("telefono"));
        return mecanico;
    }


}
