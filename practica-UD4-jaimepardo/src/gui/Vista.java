package gui;

import com.github.lgooddatepicker.components.DatePicker;
import enums.Marcas;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * Clase que crea el frame de Vista
 */
public class Vista extends JFrame {
    private final static String TITULOFRAME="Aplicación Recambios Jaime Pardo";
    JTabbedPane tabbedPane;
    public JPanel panel1;
    JPanel JPanelCoche;
    JPanel JPanelCliente;
    JPanel JPanelMecanico;
    JPanel JPanelRecambio;

    //Coches
    JRadioButton hibridoRadioButton;
    JRadioButton electricoRadioButton;
    JRadioButton combustionRadioButton;
    JTextField txtMatricula;
    DatePicker fecha;
    JComboBox comboCliente;
    ArrayList<JLabel> cbtRecambio;
    JLabel txtRecambio1;
    ArrayList<JComboBox> cbRecambio;
    JComboBox comboRecambio1;
    JButton btnCocheAnadir;
    JButton btnCocheModificar;
    JButton btnCocheEliminar;
    JList cochesTabla;
    JList mecanicoCocheTabla;
    JList recambioCocheTabla;


    //CLIENTES
    JTextField txtDni;
    JTextField txtEmail;
    JTextField txtNombre;
    JTextField txtApellidos;
    JTextField txtTelefono;
    JList clienteTabla;
    JButton btnClienteEliminar;
    JButton btnClienteAnadir;
    JButton btnClienteModificar;

    //MECANICOS
    JTextField txtNombreMecanico;
    JTextField txtApellidoMecanico;
    JTextField txtTelefonoMecanico;
    JList mecanicoTabla;
    JButton btnMecanicoEliminar;
    JButton btnMecanicoAnadir;
    JButton btnMecanicoModificar;
    ArrayList<JLabel> cbtMecanico;
    JLabel txtMecanico1;
    ArrayList<JComboBox> cbMecanico;
    JComboBox comboMecanico1;
    JComboBox comboMarca;

    //Recambios

    JRadioButton combustionRadioButton1;
    JRadioButton electricoRadioButton1;
    JTextField txtRecambioNombre;
    JTextField txtRecambioPrecio;
    JList recambioTabla;
    JButton btnRecambioAnadir;
    JButton btnRecambioModificar;
    JButton btnEliminarRecambio;


    JScrollPane panelMecanicos;
    JScrollPane panelRecambios;
    JLabel tituloMecanicos;
    JLabel tituloRecambios;
    JTextField txtBusquedaCoche;
    JList listaBusqueda;
    JTextField txtBusqueda;

    //BUSQUEDA
    JLabel etiquetaEstado;

    //DEFAULT TABLE MODEL
    DefaultListModel dtmCoches;
    DefaultListModel dtmClientes;
    DefaultListModel dtmMecanicos;
    DefaultListModel dtmRecambios;
    DefaultListModel dtmRecambioCoche;
    DefaultListModel dtmMecanicoCoche;
    DefaultListModel dtmBusqueda;

    //BARRA MENU
    JMenuItem itemOpciones;
    JMenuItem itemDesconectar;
    JMenuItem itemSalir;


    public Vista() {
        initFrame();
    }

    /**
     * Inicia los componentes del frames
     */
    public void initFrame() {
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setResizable(false);
        this.setSize(900,500);
        this.setVisible(true);
        //creo cuadro de dialogo
        setMenu();
        setEnumComboBox();
        setTableModels();

        this.panel1.setVisible(false);
    }

    /**
     * Asigna los table models a las tablas de la vista
     */
    private void setTableModels() {
        this.dtmCoches=new DefaultListModel();
        this.cochesTabla.setModel(dtmCoches);
        this.dtmMecanicos=new DefaultListModel();
        this.mecanicoTabla.setModel(dtmMecanicos);
        this.dtmClientes=new DefaultListModel();
        this.clienteTabla.setModel(dtmClientes);
        this.dtmRecambios=new DefaultListModel();
        this.recambioTabla.setModel(dtmRecambios);
        this.recambioCocheTabla.setEnabled(false);
        this.dtmRecambioCoche = new DefaultListModel();
        this.recambioCocheTabla.setModel(dtmRecambioCoche);
        this.mecanicoCocheTabla.setEnabled(false);
        this.dtmMecanicoCoche = new DefaultListModel();
        this.mecanicoCocheTabla.setModel(dtmMecanicoCoche);
        this.dtmBusqueda = new DefaultListModel();
        this.listaBusqueda.setModel(dtmBusqueda);
    }

    /**
     * Asigna los componentes del menu de opcioness
     */
    private void setMenu() {
        JMenuBar mbBar = new JMenuBar();
        JMenu menu = new JMenu("Archivo");
        //por cada item que tenga funcionalidad tiene un ActionCommand
        itemOpciones= new JMenuItem("Conectar");
        itemOpciones.setActionCommand("Conectar");
        itemDesconectar= new JMenuItem("Desconectar");
        itemDesconectar.setActionCommand("Desconectar");
        itemSalir=new JMenuItem("Salir");
        itemSalir.setActionCommand("Salir");
        menu.add(itemOpciones);
        menu.add(itemDesconectar);
        menu.add(itemSalir);
        mbBar.add(menu);
        //centrar en horizontal
        mbBar.add(Box.createHorizontalGlue());
        this.setJMenuBar(mbBar);
    }

    /**
     * Asigna los enumerables al combobox.
     */
    private void setEnumComboBox() {
        for (Marcas m: Marcas.values()) {
            comboMarca.addItem(m.getValor());
        }
        comboMarca.setSelectedIndex(-1);
    }



    public void agrandarPantalla(){
        this.setSize(1180,500);
    }

    public void encogerPantalla() {
        this.setSize(900,500);
    }

}
