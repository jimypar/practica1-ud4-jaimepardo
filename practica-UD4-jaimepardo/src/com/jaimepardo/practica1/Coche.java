package com.jaimepardo.practica1;

import org.bson.types.ObjectId;

import java.time.LocalDate;
import java.util.Objects;

public class Coche {
    private ObjectId id;
    private String tipo;
    private String matricula;
    private String marca;
    private LocalDate fechaAlta;
    private ObjectId clienteId;
    private ObjectId recambioId;
    private ObjectId mecanicoId;

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public LocalDate getFechaAlta() {
        return fechaAlta;
    }

    public void setFechaAlta(LocalDate fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public ObjectId getClienteId() {
        return clienteId;
    }

    public void setClienteId(ObjectId clienteId) {
        this.clienteId = clienteId;
    }

    public ObjectId getRecambioId() {
        return recambioId;
    }

    public void setRecambioId(ObjectId recambioId) {
        this.recambioId = recambioId;
    }

    public ObjectId getMecanicoId() {
        return mecanicoId;
    }

    public void setMecanicoId(ObjectId mecanicoId) {
        this.mecanicoId = mecanicoId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Coche coche = (Coche) o;
        return id == coche.id &&
                Objects.equals(tipo, coche.tipo) &&
                Objects.equals(matricula, coche.matricula) &&
                Objects.equals(marca, coche.marca) &&
                Objects.equals(fechaAlta, coche.fechaAlta);
    }

    @Override
    public String toString() {
        return "["+matricula+"]"+" "+marca+" , "+tipo;
    }
}
