package com.jaimepardo.practica1;


import org.bson.types.ObjectId;

import java.util.Objects;

public class Recambio {
    private ObjectId id;
    private String componente;
    private Double precio;
    private Boolean combustion;
    private Boolean electrico;

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getComponente() {
        return componente;
    }

    public void setComponente(String componente) {
        this.componente = componente;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    public Boolean getCombustion() {
        return combustion;
    }

    public void setCombustion(Boolean combustion) {
        this.combustion = combustion;
    }

    public Boolean getElectrico() {
        return electrico;
    }

    public void setElectrico(Boolean electrico) {
        this.electrico = electrico;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Recambio recambio = (Recambio) o;
        return id == recambio.id &&
                Objects.equals(componente, recambio.componente) &&
                Objects.equals(precio, recambio.precio) &&
                Objects.equals(combustion, recambio.combustion) &&
                Objects.equals(electrico, recambio.electrico);
    }

    @Override
    public String toString() {
        return componente + " " + precio + " $";
    }
}
