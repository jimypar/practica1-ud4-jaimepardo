package com.jaimepardo.practica1;

import org.bson.types.ObjectId;

import java.util.Objects;

public class Mecanico {
    private ObjectId id;
    private String nombre;
    private String apellidos;
    private int telefono;


    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Mecanico mecanico = (Mecanico) o;
        return id == mecanico.id &&
                telefono == mecanico.telefono &&
                Objects.equals(nombre, mecanico.nombre) &&
                Objects.equals(apellidos, mecanico.apellidos);
    }

    @Override
    public String toString() {
        return nombre + ", " + apellidos;
    }
}
